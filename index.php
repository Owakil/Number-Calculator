<?php

include_once 'vendor/autoload.php';

use Pondit\Calculator\NumberCalculator\Addition;
use Pondit\Calculator\NumberCalculator\Substraction;
use Pondit\Calculator\NumberCalculator\Multiplication;
use Pondit\Calculator\NumberCalculator\Division;

$addition1=new Addition(2,3);
$addition2=new Addition(5,4);
var_dump($addition1);
var_dump($addition2);

$substraction1 = new Substraction();
var_dump($substraction1);

$multiplication1 = new Multiplication();
var_dump($multiplication1);

$division1 = new Division();
var_dump($division1);